#!/usr/bin/env bash
set -e
export GHC=$HOME/ghcs-nix/ghcs/9.6.3/bin/ghc
export LD_LIBRARY_PATH=$(pwd)/result/lib:$LD_LIBRARY_PATH

rm smaps/*

exe=$(cabal list-bin -w $GHC $@ T23949)
nix run nixpkgs#k6 -- run --vus 100 --duration 100m run.js &

python ~/ghc/ghc-utils/sample_proc.py --smaps=10 -m VmStk -m VmData -m RssAnon -m VmRSS -m VmHWM -s 2 -o log -- $exe +RTS -l -N10

kill -TERM %1

